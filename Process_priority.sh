#!/usr/bin/env bash

#1
mkdir pipes ; cd pipes/

 mkfifo p1 p2 p3 p4 p5 p6

#2

echo -n x | cat - p1 > p2 &

cat <p2 >p1 $

#3

top

 ps -C cat

#4

echo -n y | nice cat - p3 > p4 &

cat <p3 >p4 &

#5

 ps -C cat -o pid,ppid,pri,ni,comm

#6

renice +15 4025

ps -C cat -o pid,ppid,pri,ni,comm



